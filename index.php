<?php 
//errors variable
$errors = "";

//db connection
$db = mysqli_connect("localhost", "root", "mysql", "practice");

//create tasks
if(isset($_POST['submit'])){
	if(empty($_POST['task'])){
		$errors = "You must fill in the task";
	}else{
		$task = $_POST['task'];
		$sql = "INSERT INTO todo_app_core_php (task) VALUES ('$task')";
		if(mysqli_query($db, $sql)){
			header('location: index.php');
		}else{
			$errors = "Something went wrong.Please try again!";
		}
	}
}

//delete tasks
if(isset($_GET['delete'])){
	$id = $_GET['delete'];

	$sql = "DELETE FROM todo_app_core_php WHERE id=".$id;
	if(mysqli_query($db, $sql)){
		header('location: index.php');
	}else{
		$errors = "Something went wrong.Please try again!";
	}
}
?>
<html>
	<head>
		<title>TODO Application Using Core PHP</title>
		<link rel="stylesheet" href="style.css" type="text/css">
	</head>
	<body>
		<div>
			<h2 align="center">TODO Application Using Core PHP</h2>
		</div>
		<form action="index.php" method="post">
			<!-- Error handling -->
			<?php if(isset($errors)){ ?>
				<p><?php echo $errors; ?></p>
			<?php } ?>
			<input type="text" name="task" class="task_input">
			<button type="submit" name="submit" class="add_btn">Add Task</button>
		</form>

		<table>
			<thead>
				<tr>
					<th>N</th>
					<th>Tasks</th>
					<th style="width: 60px;">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				//retrieve all data
				$tasks = mysqli_query($db, "SELECT * from todo_app_core_php");
				$i = 1;
				while($row = mysqli_fetch_array($tasks)) { 
				?>
                 <tr>
                 	<td><?php echo $i; ?></td>
                 	<td class="task"><?php echo $row['task']; ?></td>
                 	<td class="delete">
                 		<a href="index.php?delete=<?php echo $row['id']; ?>">x</a>
                 	</td>
                 </tr>
			    <?php $i++; } ?>
			</tbody>
		</table>
	</body>
</html>